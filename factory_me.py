#Exercicio Factory
class Tipos:
    def __init__(self, me=None):
        self.me = me

    def who_am_i(self, me):
        self.me = me.upper()
        if self.me in ['R2D2', 'C3PO', 'BB8']:
            return Inception()
        else:
            return Inception_do_Inception()

    def am_i(self):
        pass

    def __repr__(self, me=None):
        return f'{self.me}'

class Inception:
    def __init__(self):
        ...

    def am_i(self):
        print('Oi, eu sou um robozinho. (:')

class Inception_do_Inception:
    def __init__(self):
        ...

    def am_i(self):
        print('Sei o que vc é não, mano.')



c3po = Tipos().who_am_i('C3PO')
c3po.am_i()
hobbit = Tipos().who_am_i('hobbit')
hobbit.am_i()
r2d2 = Tipos().who_am_i('r2d2')
r2d2.am_i()
